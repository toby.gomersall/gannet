use crate::{
    errors::DeviceError,
    multi_channel::registers::Registers,
    TransferType,
};

/// The maximum number of channels that the DMA engine can support. Note that
/// you will only be able to use the number of channels configured in the
/// FPGA, which will be up to `MAX_CHANNELS`.
pub const MAX_CHANNELS: u8 = 16;

// Return the correct register for the channel control register based on
// transfer type and channel.
#[inline]
pub(crate) fn ch_control_reg(
    transfer_type: &TransferType, channel: &u8)-> Result<Registers, DeviceError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0Control),
                1 => Ok(Registers::Mm2sCh1Control),
                2 => Ok(Registers::Mm2sCh2Control),
                3 => Ok(Registers::Mm2sCh3Control),
                4 => Ok(Registers::Mm2sCh4Control),
                5 => Ok(Registers::Mm2sCh5Control),
                6 => Ok(Registers::Mm2sCh6Control),
                7 => Ok(Registers::Mm2sCh7Control),
                8 => Ok(Registers::Mm2sCh8Control),
                9 => Ok(Registers::Mm2sCh9Control),
                10 => Ok(Registers::Mm2sCh10Control),
                11 => Ok(Registers::Mm2sCh11Control),
                12 => Ok(Registers::Mm2sCh12Control),
                13 => Ok(Registers::Mm2sCh13Control),
                14 => Ok(Registers::Mm2sCh14Control),
                15 => Ok(Registers::Mm2sCh15Control),
                _ => return Err(DeviceError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0Control),
                1 => Ok(Registers::S2mmCh1Control),
                2 => Ok(Registers::S2mmCh2Control),
                3 => Ok(Registers::S2mmCh3Control),
                4 => Ok(Registers::S2mmCh4Control),
                5 => Ok(Registers::S2mmCh5Control),
                6 => Ok(Registers::S2mmCh6Control),
                7 => Ok(Registers::S2mmCh7Control),
                8 => Ok(Registers::S2mmCh8Control),
                9 => Ok(Registers::S2mmCh9Control),
                10 => Ok(Registers::S2mmCh10Control),
                11 => Ok(Registers::S2mmCh11Control),
                12 => Ok(Registers::S2mmCh12Control),
                13 => Ok(Registers::S2mmCh13Control),
                14 => Ok(Registers::S2mmCh14Control),
                15 => Ok(Registers::S2mmCh15Control),
                _ => return Err(DeviceError::InvalidChannel),
            }
        }
    }
}

// Return the correct register for the channel status register based on
// transfer type and channel.
#[inline]
pub(crate) fn ch_status_reg(
    transfer_type: &TransferType, channel: &u8)-> Result<Registers, DeviceError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0Status),
                1 => Ok(Registers::Mm2sCh1Status),
                2 => Ok(Registers::Mm2sCh2Status),
                3 => Ok(Registers::Mm2sCh3Status),
                4 => Ok(Registers::Mm2sCh4Status),
                5 => Ok(Registers::Mm2sCh5Status),
                6 => Ok(Registers::Mm2sCh6Status),
                7 => Ok(Registers::Mm2sCh7Status),
                8 => Ok(Registers::Mm2sCh8Status),
                9 => Ok(Registers::Mm2sCh9Status),
                10 => Ok(Registers::Mm2sCh10Status),
                11 => Ok(Registers::Mm2sCh11Status),
                12 => Ok(Registers::Mm2sCh12Status),
                13 => Ok(Registers::Mm2sCh13Status),
                14 => Ok(Registers::Mm2sCh14Status),
                15 => Ok(Registers::Mm2sCh15Status),
                _ => return Err(DeviceError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0Status),
                1 => Ok(Registers::S2mmCh1Status),
                2 => Ok(Registers::S2mmCh2Status),
                3 => Ok(Registers::S2mmCh3Status),
                4 => Ok(Registers::S2mmCh4Status),
                5 => Ok(Registers::S2mmCh5Status),
                6 => Ok(Registers::S2mmCh6Status),
                7 => Ok(Registers::S2mmCh7Status),
                8 => Ok(Registers::S2mmCh8Status),
                9 => Ok(Registers::S2mmCh9Status),
                10 => Ok(Registers::S2mmCh10Status),
                11 => Ok(Registers::S2mmCh11Status),
                12 => Ok(Registers::S2mmCh12Status),
                13 => Ok(Registers::S2mmCh13Status),
                14 => Ok(Registers::S2mmCh14Status),
                15 => Ok(Registers::S2mmCh15Status),
                _ => return Err(DeviceError::InvalidChannel),
            }
        }
    }
}

// Return the correct register for the channel current descriptor LSW register
// based on transfer type and channel.
#[inline]
pub(crate) fn ch_current_descriptor_lsw_reg(
    transfer_type: &TransferType, channel: &u8)-> Result<Registers, DeviceError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0CurrentDescriptorLsw),
                1 => Ok(Registers::Mm2sCh1CurrentDescriptorLsw),
                2 => Ok(Registers::Mm2sCh2CurrentDescriptorLsw),
                3 => Ok(Registers::Mm2sCh3CurrentDescriptorLsw),
                4 => Ok(Registers::Mm2sCh4CurrentDescriptorLsw),
                5 => Ok(Registers::Mm2sCh5CurrentDescriptorLsw),
                6 => Ok(Registers::Mm2sCh6CurrentDescriptorLsw),
                7 => Ok(Registers::Mm2sCh7CurrentDescriptorLsw),
                8 => Ok(Registers::Mm2sCh8CurrentDescriptorLsw),
                9 => Ok(Registers::Mm2sCh9CurrentDescriptorLsw),
                10 => Ok(Registers::Mm2sCh10CurrentDescriptorLsw),
                11 => Ok(Registers::Mm2sCh11CurrentDescriptorLsw),
                12 => Ok(Registers::Mm2sCh12CurrentDescriptorLsw),
                13 => Ok(Registers::Mm2sCh13CurrentDescriptorLsw),
                14 => Ok(Registers::Mm2sCh14CurrentDescriptorLsw),
                15 => Ok(Registers::Mm2sCh15CurrentDescriptorLsw),
                _ => return Err(DeviceError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0CurrentDescriptorLsw),
                1 => Ok(Registers::S2mmCh1CurrentDescriptorLsw),
                2 => Ok(Registers::S2mmCh2CurrentDescriptorLsw),
                3 => Ok(Registers::S2mmCh3CurrentDescriptorLsw),
                4 => Ok(Registers::S2mmCh4CurrentDescriptorLsw),
                5 => Ok(Registers::S2mmCh5CurrentDescriptorLsw),
                6 => Ok(Registers::S2mmCh6CurrentDescriptorLsw),
                7 => Ok(Registers::S2mmCh7CurrentDescriptorLsw),
                8 => Ok(Registers::S2mmCh8CurrentDescriptorLsw),
                9 => Ok(Registers::S2mmCh9CurrentDescriptorLsw),
                10 => Ok(Registers::S2mmCh10CurrentDescriptorLsw),
                11 => Ok(Registers::S2mmCh11CurrentDescriptorLsw),
                12 => Ok(Registers::S2mmCh12CurrentDescriptorLsw),
                13 => Ok(Registers::S2mmCh13CurrentDescriptorLsw),
                14 => Ok(Registers::S2mmCh14CurrentDescriptorLsw),
                15 => Ok(Registers::S2mmCh15CurrentDescriptorLsw),
                _ => return Err(DeviceError::InvalidChannel),
            }
        }
    }
}

// Return the correct register for the channel current descriptor MSW register
// based on transfer type and channel.
#[inline]
pub(crate) fn ch_current_descriptor_msw_reg(
    transfer_type: &TransferType, channel: &u8)-> Result<Registers, DeviceError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0CurrentDescriptorMsw),
                1 => Ok(Registers::Mm2sCh1CurrentDescriptorMsw),
                2 => Ok(Registers::Mm2sCh2CurrentDescriptorMsw),
                3 => Ok(Registers::Mm2sCh3CurrentDescriptorMsw),
                4 => Ok(Registers::Mm2sCh4CurrentDescriptorMsw),
                5 => Ok(Registers::Mm2sCh5CurrentDescriptorMsw),
                6 => Ok(Registers::Mm2sCh6CurrentDescriptorMsw),
                7 => Ok(Registers::Mm2sCh7CurrentDescriptorMsw),
                8 => Ok(Registers::Mm2sCh8CurrentDescriptorMsw),
                9 => Ok(Registers::Mm2sCh9CurrentDescriptorMsw),
                10 => Ok(Registers::Mm2sCh10CurrentDescriptorMsw),
                11 => Ok(Registers::Mm2sCh11CurrentDescriptorMsw),
                12 => Ok(Registers::Mm2sCh12CurrentDescriptorMsw),
                13 => Ok(Registers::Mm2sCh13CurrentDescriptorMsw),
                14 => Ok(Registers::Mm2sCh14CurrentDescriptorMsw),
                15 => Ok(Registers::Mm2sCh15CurrentDescriptorMsw),
                _ => return Err(DeviceError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0CurrentDescriptorMsw),
                1 => Ok(Registers::S2mmCh1CurrentDescriptorMsw),
                2 => Ok(Registers::S2mmCh2CurrentDescriptorMsw),
                3 => Ok(Registers::S2mmCh3CurrentDescriptorMsw),
                4 => Ok(Registers::S2mmCh4CurrentDescriptorMsw),
                5 => Ok(Registers::S2mmCh5CurrentDescriptorMsw),
                6 => Ok(Registers::S2mmCh6CurrentDescriptorMsw),
                7 => Ok(Registers::S2mmCh7CurrentDescriptorMsw),
                8 => Ok(Registers::S2mmCh8CurrentDescriptorMsw),
                9 => Ok(Registers::S2mmCh9CurrentDescriptorMsw),
                10 => Ok(Registers::S2mmCh10CurrentDescriptorMsw),
                11 => Ok(Registers::S2mmCh11CurrentDescriptorMsw),
                12 => Ok(Registers::S2mmCh12CurrentDescriptorMsw),
                13 => Ok(Registers::S2mmCh13CurrentDescriptorMsw),
                14 => Ok(Registers::S2mmCh14CurrentDescriptorMsw),
                15 => Ok(Registers::S2mmCh15CurrentDescriptorMsw),
                _ => return Err(DeviceError::InvalidChannel),
            }
        }
    }
}

// Return the correct register for the channel tail descriptor LSW register
// based on transfer type and channel.
#[inline]
pub(crate) fn ch_tail_descriptor_lsw_reg(
    transfer_type: &TransferType, channel: &u8)-> Result<Registers, DeviceError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0TailDescriptorLsw),
                1 => Ok(Registers::Mm2sCh1TailDescriptorLsw),
                2 => Ok(Registers::Mm2sCh2TailDescriptorLsw),
                3 => Ok(Registers::Mm2sCh3TailDescriptorLsw),
                4 => Ok(Registers::Mm2sCh4TailDescriptorLsw),
                5 => Ok(Registers::Mm2sCh5TailDescriptorLsw),
                6 => Ok(Registers::Mm2sCh6TailDescriptorLsw),
                7 => Ok(Registers::Mm2sCh7TailDescriptorLsw),
                8 => Ok(Registers::Mm2sCh8TailDescriptorLsw),
                9 => Ok(Registers::Mm2sCh9TailDescriptorLsw),
                10 => Ok(Registers::Mm2sCh10TailDescriptorLsw),
                11 => Ok(Registers::Mm2sCh11TailDescriptorLsw),
                12 => Ok(Registers::Mm2sCh12TailDescriptorLsw),
                13 => Ok(Registers::Mm2sCh13TailDescriptorLsw),
                14 => Ok(Registers::Mm2sCh14TailDescriptorLsw),
                15 => Ok(Registers::Mm2sCh15TailDescriptorLsw),
                _ => return Err(DeviceError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0TailDescriptorLsw),
                1 => Ok(Registers::S2mmCh1TailDescriptorLsw),
                2 => Ok(Registers::S2mmCh2TailDescriptorLsw),
                3 => Ok(Registers::S2mmCh3TailDescriptorLsw),
                4 => Ok(Registers::S2mmCh4TailDescriptorLsw),
                5 => Ok(Registers::S2mmCh5TailDescriptorLsw),
                6 => Ok(Registers::S2mmCh6TailDescriptorLsw),
                7 => Ok(Registers::S2mmCh7TailDescriptorLsw),
                8 => Ok(Registers::S2mmCh8TailDescriptorLsw),
                9 => Ok(Registers::S2mmCh9TailDescriptorLsw),
                10 => Ok(Registers::S2mmCh10TailDescriptorLsw),
                11 => Ok(Registers::S2mmCh11TailDescriptorLsw),
                12 => Ok(Registers::S2mmCh12TailDescriptorLsw),
                13 => Ok(Registers::S2mmCh13TailDescriptorLsw),
                14 => Ok(Registers::S2mmCh14TailDescriptorLsw),
                15 => Ok(Registers::S2mmCh15TailDescriptorLsw),
                _ => return Err(DeviceError::InvalidChannel),
            }
        }
    }
}

// Return the correct register for the channel tail descriptor MSW register
// based on transfer type and channel.
#[inline]
pub(crate) fn ch_tail_descriptor_msw_reg(
    transfer_type: &TransferType, channel: &u8)-> Result<Registers, DeviceError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0TailDescriptorMsw),
                1 => Ok(Registers::Mm2sCh1TailDescriptorMsw),
                2 => Ok(Registers::Mm2sCh2TailDescriptorMsw),
                3 => Ok(Registers::Mm2sCh3TailDescriptorMsw),
                4 => Ok(Registers::Mm2sCh4TailDescriptorMsw),
                5 => Ok(Registers::Mm2sCh5TailDescriptorMsw),
                6 => Ok(Registers::Mm2sCh6TailDescriptorMsw),
                7 => Ok(Registers::Mm2sCh7TailDescriptorMsw),
                8 => Ok(Registers::Mm2sCh8TailDescriptorMsw),
                9 => Ok(Registers::Mm2sCh9TailDescriptorMsw),
                10 => Ok(Registers::Mm2sCh10TailDescriptorMsw),
                11 => Ok(Registers::Mm2sCh11TailDescriptorMsw),
                12 => Ok(Registers::Mm2sCh12TailDescriptorMsw),
                13 => Ok(Registers::Mm2sCh13TailDescriptorMsw),
                14 => Ok(Registers::Mm2sCh14TailDescriptorMsw),
                15 => Ok(Registers::Mm2sCh15TailDescriptorMsw),
                _ => return Err(DeviceError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0TailDescriptorMsw),
                1 => Ok(Registers::S2mmCh1TailDescriptorMsw),
                2 => Ok(Registers::S2mmCh2TailDescriptorMsw),
                3 => Ok(Registers::S2mmCh3TailDescriptorMsw),
                4 => Ok(Registers::S2mmCh4TailDescriptorMsw),
                5 => Ok(Registers::S2mmCh5TailDescriptorMsw),
                6 => Ok(Registers::S2mmCh6TailDescriptorMsw),
                7 => Ok(Registers::S2mmCh7TailDescriptorMsw),
                8 => Ok(Registers::S2mmCh8TailDescriptorMsw),
                9 => Ok(Registers::S2mmCh9TailDescriptorMsw),
                10 => Ok(Registers::S2mmCh10TailDescriptorMsw),
                11 => Ok(Registers::S2mmCh11TailDescriptorMsw),
                12 => Ok(Registers::S2mmCh12TailDescriptorMsw),
                13 => Ok(Registers::S2mmCh13TailDescriptorMsw),
                14 => Ok(Registers::S2mmCh14TailDescriptorMsw),
                15 => Ok(Registers::S2mmCh15TailDescriptorMsw),
                _ => return Err(DeviceError::InvalidChannel),
            }
        }
    }
}

/*
 * Userspace I/O platform driver with generic IRQ handling code and memory
 * allocation as specified in device tree.
 *
 * Memory is always allocated to the device at probe time based on device
 * tree values (i.e. from the device tree settings, blocks of memory are
 * effectively statically allocated to this device). This means the running
 * or not of the userspace code does not have an impact on the ability of the
 * hardware to have a valid location to read-from and write-to.
 *
 * Based on the various in-tree UIO modules, notably uio_pdr_genirq and
 * uio_dmem_genirq, and subsequently heavily modified.
 *
 * Might be worth considering for allocations of the buffer regions with
 * more flexibility (not related to this driver):
 * https://github.com/ikwzm/udmabuf
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

#include <linux/platform_device.h>
#include <linux/uio_driver.h>
#include <linux/spinlock.h>
#include <linux/bitops.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/stringify.h>
#include <linux/slab.h>
#include <linux/dma-mapping.h>

#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/of_address.h>

#define DRIVER_NAME "axi_irq_mem"

struct axi_irq_mem_platdata {
	struct uio_info *uioinfo;
	spinlock_t lock;
	unsigned long flags;
	struct platform_device *pdev;
	int iomem_nb;
	char *data_mem_addr;
	dma_addr_t data_mem_handle;
    size_t data_mem_size;
    char *desc_mem_addr;
	dma_addr_t desc_mem_handle;
    size_t desc_mem_size;
};

/* Bits in axi_irq_mem_platdata.flags */
enum {
	UIO_IRQ_DISABLED = 0,
};

static int axi_irq_mem_open(struct uio_info *info, struct inode *inode)
{
	return 0;
}

static int axi_irq_mem_release(struct uio_info *info, struct inode *inode)
{
	return 0;
}

static irqreturn_t axi_irq_mem_handler(int irq, struct uio_info *dev_info)
{
	struct axi_irq_mem_platdata *priv = dev_info->priv;

    /* FIXME remove the disabling of the interrupt controller. This is
     * probably not necessary and means the driver has several extra syscalls
     * per operation (@~2microseconds per syscall). Some care is needed as
     * the userspace code currently relies on the number of interrupts being
     * zero to denote a timeout. The behaviour needs to be checked on this
     * and the userspace code updated accordingly.
     * */

	/* Just disable the interrupt in the interrupt controller, and
	 * remember the state so we can allow user space to enable it later.
	 */

	spin_lock(&priv->lock);
	if (!__test_and_set_bit(UIO_IRQ_DISABLED, &priv->flags))
		disable_irq_nosync(irq);
	spin_unlock(&priv->lock);

	return IRQ_HANDLED;
}

static int axi_irq_mem_irqcontrol(struct uio_info *dev_info, s32 irq_on)
{
    /* FIXME see axi_irq_mem_handler. If the interrupt is never disabled,
     * this whole function should be removed and disabled in the driver since
     * it would no longer be needed (interrupts would always run).
     * */

	struct axi_irq_mem_platdata *priv = dev_info->priv;
	unsigned long flags;

	/* Allow user space to enable and disable the interrupt
	 * in the interrupt controller, but keep track of the
	 * state to prevent per-irq depth damage.
	 *
	 * Serialize this operation to support multiple tasks and concurrency
	 * with irq handler on SMP systems.
	 */

	spin_lock_irqsave(&priv->lock, flags);
	if (irq_on) {
		if (__test_and_clear_bit(UIO_IRQ_DISABLED, &priv->flags))
			enable_irq(dev_info->irq);
	} else {
		if (!__test_and_set_bit(UIO_IRQ_DISABLED, &priv->flags))
			disable_irq_nosync(dev_info->irq);
	}
	spin_unlock_irqrestore(&priv->lock, flags);

	return 0;
}

static int axi_irq_find_mem_index(struct uio_info *info,
				      struct vm_area_struct *vma)
{
	if (vma->vm_pgoff < MAX_UIO_MAPS) {
		if (info->mem[vma->vm_pgoff].size == 0)
			return -1;
		return (int)vma->vm_pgoff;
	}
	return -1;
}

static const struct vm_operations_struct uio_physical_vm_ops = {
#ifdef CONFIG_HAVE_IOREMAP_PROT
	.access = generic_access_phys,
#endif
};

static int axi_irq_mmap(struct uio_info *info,
        struct vm_area_struct *vma)
{
    /* Ripped out from uio.c, replacing uio_mmap_physical function
     * with this one which sets up the memory region as writecombine
     */
	int mi = axi_irq_find_mem_index(info, vma);
	struct uio_mem *mem;

	if (mi < 0)
		return -EINVAL;
	mem = info->mem + mi;

	if (mem->addr & ~PAGE_MASK)
		return -ENODEV;
	if (vma->vm_end - vma->vm_start > mem->size)
		return -EINVAL;

    vma->vm_ops = &uio_physical_vm_ops;

    /* Options for seting the page protections on ARM:
     * pgprot_noncached is strongly-ordered, uncached, unbuffered ("Strongly
     * ordered" mem)
     * pgprot_device is strongly-ordered, uncached, buffered ("Device" mem)
     * pgprot_writecombine is weakly-ordered, uncached, buffered ("Normal" mem)
     * pgprot_dmacoherent ? weakly-ordered, buffered, cached? ("Normal" mem)
     * Other options are availble, that covers the main ones of interest
     * (buffered implies writes are buffered and combined)
     */
    vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
    /* FIXME use the following logic to improve performance on the memory
     * backed regions.
    if (mi < 1)
        vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
	else
		vma->vm_page_prot = pgprot_writecombine(vma->vm_page_prot);
    */

	/*
	 * We cannot use the vm_iomap_memory() helper here,
	 * because vma->vm_pgoff is the map index we looked
	 * up above in uio_find_mem_index(), rather than an
	 * actual page offset into the mmap.
	 *
	 * So we just do the physical mmap without a page
	 * offset.
	 */
	return remap_pfn_range(vma,
			       vma->vm_start,
			       mem->addr >> PAGE_SHIFT,
			       vma->vm_end - vma->vm_start,
			       vma->vm_page_prot);
}

static int axi_irq_mem_probe(struct platform_device *pdev)
{
	struct uio_info *uioinfo = dev_get_platdata(&pdev->dev);
	struct axi_irq_mem_platdata *priv;
	struct uio_mem *uiomem;
	int ret = -EINVAL;
	int iomem_nb = 0;
	int i;

    const __be32 *be_dma_data_mem_size;
    size_t dma_data_mem_size;

    const __be32 *be_dma_descriptors_mem_size;
    size_t dma_descriptors_mem_size;

	if (pdev->dev.of_node) {
		/* alloc uioinfo for one device */
		uioinfo = devm_kzalloc(&pdev->dev, sizeof(*uioinfo),
				       GFP_KERNEL);
		if (!uioinfo) {
			dev_err(&pdev->dev, "unable to kmalloc\n");
			return -ENOMEM;
		}
		uioinfo->name = pdev->dev.of_node->name;
		uioinfo->version = "devicetree";
	}

	if (!uioinfo || !uioinfo->name || !uioinfo->version) {
		dev_err(&pdev->dev, "missing platform_data\n");
		return ret;
	}

	if (uioinfo->handler || uioinfo->irqcontrol ||
	    uioinfo->irq_flags & IRQF_SHARED) {
		dev_err(&pdev->dev, "interrupt configuration error\n");
		return ret;
	}

	priv = devm_kzalloc(&pdev->dev, sizeof(*priv), GFP_KERNEL);
	if (!priv) {
		dev_err(&pdev->dev, "unable to kmalloc\n");
		return -ENOMEM;
	}

    /* For the moment configure the device to use a 32-bit wide address.
     * It might be necessary to change this if using a 64-bit system and
     * the device is configured to use more of those address bits.
     *
     * We're only using coherent mappings so leave the stream setting as the
     * default.
     * */
	dma_set_coherent_mask(&pdev->dev, DMA_BIT_MASK(32));

	priv->uioinfo = uioinfo;
	spin_lock_init(&priv->lock);
	priv->flags = 0; /* interrupt is enabled to begin with */
	priv->pdev = pdev;

    /* Multiple IRQs are not supported */
    if (!uioinfo->irq) {
		ret = platform_get_irq(pdev, 0);
		uioinfo->irq = ret;
		if (ret == -ENXIO && pdev->dev.of_node)
			uioinfo->irq = UIO_IRQ_NONE;
		else if (ret < 0) {
			dev_err(&pdev->dev, "failed to get IRQ\n");
			return ret;
		}
	}

	uiomem = &uioinfo->mem[0];

	for (i = 0; i < pdev->num_resources; ++i) {
		struct resource *r = &pdev->resource[i];

		if (r->flags != IORESOURCE_MEM)
			continue;

		if (uiomem >= &uioinfo->mem[MAX_UIO_MAPS]) {
			dev_warn(&pdev->dev, "device has more than "
					__stringify(MAX_UIO_MAPS)
					" I/O memory resources.\n");
			break;
		}

		uiomem->memtype = UIO_MEM_PHYS;
		uiomem->addr = r->start;
		uiomem->size = resource_size(r);
		uiomem->name = r->name;

		if (uiomem->size != 0) {
			if (!request_mem_region
			    (uiomem->addr, uiomem->size, DRIVER_NAME)) {
				dev_err(&pdev->dev,
					"request_mem_region failed. Aborting.\n");
				ret = -EBUSY;
				goto bad0;
			}
		}

		++iomem_nb;
		++uiomem;
	}
	priv->iomem_nb = iomem_nb;

    /* Device tree is read as big endian */
    be_dma_data_mem_size = of_get_property(
            pdev->dev.of_node, "dma-data-mem-size", NULL);

    if (!be_dma_data_mem_size) {
        /* extra-mem-size is the deprecated name */
        be_dma_data_mem_size = of_get_property(
                pdev->dev.of_node, "extra-mem-size", NULL);

        if (be_dma_data_mem_size) {
            printk(KERN_WARNING "axi-irq-mem: extra-mem-size is deprecated. "
                    "Please use dma-data-mem-size in your device tree.");
        }
    }

    if (be_dma_data_mem_size) {
        /* Make sure the dma_data_mem_size value matches the endianness of
         * the CPU */
        dma_data_mem_size = be32_to_cpup(be_dma_data_mem_size);
	    /* needs a free mem array position for CMA */
	    if (uiomem >= &uioinfo->mem[MAX_UIO_MAPS]) {
    		dev_err(&pdev->dev, "device has more than "
  			    __stringify(MAX_UIO_MAPS)
			    " CMA and fixed memory regions.\n");
		    ret = -EBUSY;
		    goto bad0;
	    }

	    /* allocate data mmap area */
	    priv->data_mem_addr = dma_alloc_coherent(
                &pdev->dev, dma_data_mem_size, &priv->data_mem_handle,
                GFP_HIGHUSER);
	    if (!priv->data_mem_addr) {
		    dev_err(&pdev->dev,
			    "allocating data memory failed. Aborting.\n");

		    ret = -ENOMEM;
		    goto bad0;
	    }

        priv->data_mem_size = dma_data_mem_size;

	    uiomem->memtype = UIO_MEM_PHYS;
	    uiomem->addr = (int) virt_to_phys(priv->data_mem_addr);
	    uiomem->size = dma_data_mem_size;
	    uiomem->name = "dma_data";
	    ++uiomem;
    }

    /* Device tree is read as big endian */
    be_dma_descriptors_mem_size = of_get_property(
            pdev->dev.of_node, "dma-descriptors-mem-size", NULL);

    if (be_dma_descriptors_mem_size) {
        /* Make sure the dma_descriptors_mem_size value matches the endianness
         * of the CPU */
        dma_descriptors_mem_size = be32_to_cpup(be_dma_descriptors_mem_size);
	    /* needs a free mem array position for CMA */
	    if (uiomem >= &uioinfo->mem[MAX_UIO_MAPS]) {
    		dev_err(&pdev->dev, "device has more than "
  			    __stringify(MAX_UIO_MAPS)
			    " CMA and fixed memory regions.\n");
		    ret = -EBUSY;
		    goto bad1;
	    }

	    /* allocate descriptors mmap area */
	    priv->desc_mem_addr = dma_alloc_coherent(
                &pdev->dev, dma_descriptors_mem_size, &priv->desc_mem_handle,
                GFP_HIGHUSER);
	    if (!priv->desc_mem_addr) {
		    dev_err(&pdev->dev,
			    "allocating descriptor memory failed. Aborting.\n");

		    ret = -ENOMEM;
		    goto bad1;
	    }

        priv->desc_mem_size = dma_descriptors_mem_size;

	    uiomem->memtype = UIO_MEM_PHYS;
	    uiomem->addr = (int) virt_to_phys(priv->desc_mem_addr);
	    uiomem->size = dma_descriptors_mem_size;
	    uiomem->name = "dma_descriptors";
	    ++uiomem;
    }


	/* mark remainder uiomem struct as empty */
	while (uiomem < &uioinfo->mem[MAX_UIO_MAPS]) {
		uiomem->size = 0;
		++uiomem;
	}

	uioinfo->handler = axi_irq_mem_handler;
	uioinfo->irqcontrol = axi_irq_mem_irqcontrol;
	uioinfo->open = axi_irq_mem_open;
	uioinfo->release = axi_irq_mem_release;
    uioinfo->mmap = axi_irq_mmap;
	uioinfo->priv = priv;

	ret = uio_register_device(&pdev->dev, priv->uioinfo);
	if (ret) {
		dev_err(&pdev->dev, "unable to register uio device\n");
		goto bad2;
	}

	platform_set_drvdata(pdev, priv);
	return 0;

 bad2:
    if (priv->desc_mem_addr) {
        dma_free_coherent(
                &pdev->dev, priv->desc_mem_size, priv->desc_mem_addr,
                priv->desc_mem_handle);
    }

 bad1:
    if (priv->data_mem_addr) {
        dma_free_coherent(
                &pdev->dev, priv->data_mem_size, priv->data_mem_addr,
                priv->data_mem_handle);
    }

 bad0:
	uiomem = &uioinfo->mem[0];
	for (i = 0; i < iomem_nb; ++i) {
		if (uiomem->size != 0) {
			release_mem_region(uiomem->addr, uiomem->size);
		}
		++uiomem;
	}
	return ret;
}

static int axi_irq_mem_remove(struct platform_device *pdev)
{
	struct axi_irq_mem_platdata *priv = platform_get_drvdata(pdev);
	struct uio_mem *uiomem;
	int i;

	/* free I/O memory areas */
	uiomem = &priv->uioinfo->mem[0];
	for (i = 0; i < priv->iomem_nb; ++i) {
		if (uiomem->size != 0) {
			release_mem_region(uiomem->addr, uiomem->size);
		}
		++uiomem;
	}

    if (priv->data_mem_addr) {
        dma_free_coherent(
                &pdev->dev, priv->data_mem_size, priv->data_mem_addr,
                priv->data_mem_handle);
    }

    if (priv->desc_mem_addr) {
        dma_free_coherent(
                &pdev->dev, priv->desc_mem_size, priv->desc_mem_addr,
                priv->desc_mem_handle);
    }

    uio_unregister_device(priv->uioinfo);

    priv->uioinfo->handler = NULL;
    priv->uioinfo->irqcontrol = NULL;

    /* kfree uioinfo for OF */
	if (pdev->dev.of_node)
		kfree(priv->uioinfo);

	kfree(priv);

    return 0;
}

static const struct of_device_id axi_irq_mem_of_match[] = {
	{.compatible = "smartacoustics,axi-irq-mem",},
	{ /* Sentinel */ },
};

MODULE_DEVICE_TABLE(of, axi_irq_mem_of_match);

static struct platform_driver axi_irq_mem = {
	.probe = axi_irq_mem_probe,
	.remove = axi_irq_mem_remove,
	.driver = {
		   .name = DRIVER_NAME,
		   .owner = THIS_MODULE,
		   .of_match_table = axi_irq_mem_of_match,
		   },
};

module_platform_driver(axi_irq_mem);

MODULE_AUTHOR("Toby Gomersall");
MODULE_DESCRIPTION("Userspace I/O platform driver for zynq AXI devices");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:" DRIVER_NAME);

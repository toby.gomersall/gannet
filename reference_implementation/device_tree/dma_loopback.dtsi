// A example device tree with suitable libgannet compatible devices created.

/ {
    // We want to restrict the space of memory the DMA system can write to.
    // We do this by setting up a block of reserved memory that the CMA
    // uses as its memory pool.
    // This address pool should be the pool that is set up in Vivado as the
    // available range it can access. The result is that there is some
    // protection against the DMA engine being able to overwrite arbitrary
    // blocks of memory.
    // There is still a risk that other drivers might use the CMA and there
    // could be some conflict at that point (by a malicious or erroneous
    // coder), but the major risk of a userspace program stepping on running
    // kernel memory should be gone.
    // This reserved-memory block is not necessary for the device driver,
    // merely a helpful tool to restrict the DMA memory space.
    reserved-memory {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;
		/* global autoconfigured region for contiguous allocations */
		linux,cma@10000000 {
			compatible = "shared-dma-pool";
			reusable;
            reg = <0x10000000 0x8000000>; // Set 128M region at 256M boundary
			linux,cma-default;
		};
	};


    // There are 8 devices:
    // * one MM2S scatter-gather DMA engine
    // * one S2MM scatter-gather DMA engine
    // * one MM2S direct DMA enginee
    // * one S2MM direct DMA engine.
    // * one MM2S and S2MM direct DMA engine
    // * one MM2S and S2MM scatter-gather DMA engine
    // * one MM2S multi channel DMA engine
    // * one S2MM multi channel DMA engine
    // In the associated PL block diagram, there is a FIFO between the
    // two scatter-gather DMA engines, there is a FIFO between
    // the two direct DMA engines, and there is a FIFO between the two multi
    // channel DMA engines. Both combined s2mm and mm2s DMA engines have FIFOs
    // between the mm2s output and s2mm input. Non-SPI level-high interrupts
    // have the interrupt property <0 N 4> where N is computed as (PL
    // interrupt number - 32).
    // Interrupt numbers can be in the ranges:
    //     61 - 68 (incl)
    //     84 - 91 (incl)
	amba {
        axi_dma_loop_sg_mm2s: axi_dma_loop_sg_mm2s@40400000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40400000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_sg_mm2s";
            interrupts = <0 29 4>; // PL Interrupt 61 (29 = 61 - 32)
            dma-data-mem-size = <0x1000000>; /* Big Endian */
            dma-descriptors-mem-size = <0x20000>; /* Big Endian */
    	};
        axi_dma_loop_sg_s2mm: axi_dma_loop_sg_s2mm@40401000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40401000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_sg_s2mm";
            interrupts = <0 30 4>; // PL Interrupt 62
            dma-data-mem-size = <0x1000000>; /* Big Endian */
            dma-descriptors-mem-size = <0x20000>; /* Big Endian */
    	};
        axi_dma_loop_direct_mm2s: axi_dma_loop_direct_mm2s@40402000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40402000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_mm2s";
            interrupts = <0 31 4>; // PL interrupt 63
            dma-data-mem-size = <0x1000000>; /* Big Endian */
    	};
        axi_dma_loop_direct_s2mm: axi_dma_loop_direct_s2mm@40403000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40403000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_s2mm";
            interrupts = <0 32 4>; // PL interrupt 64
            dma-data-mem-size = <0x1000000>; /* Big Endian */
    	};
        axi_dma_loop_combined: axi_dma_loop_combined@40404000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40404000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_combined";
            interrupts = <0 33 4>; // PL interrupt 65 (Also connected to interrupt 66 but driver can't handle 2 interrupts)
            dma-data-mem-size = <0x1000>; /* Big Endian */
    	};
        axi_dma_loop_sg_combined: axi_dma_loop_sg_combined@40405000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40405000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_sg_combined";
            interrupts = <0 35 4>; // PL interrupt 67 (Also connected to interrupt 68 but driver can't handle 2 interrupts)
            dma-data-mem-size = <0x1000>; /* Big Endian */
            dma-descriptors-mem-size = <0x1000>; /* Big Endian */
    	};
        axi_dma_loop_mc_mm2s: axi_dma_loop_mc_mm2s@40406000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40406000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_mc_mm2s";
            interrupts = <0 52 4>; // PL Interrupt 84 (52 = 84 - 32)
            dma-data-mem-size = <0x1000000>; /* Big Endian */
            dma-descriptors-mem-size = <0x40000>; /* Big Endian */
    	};
        axi_dma_loop_mc_s2mm: axi_dma_loop_mc_s2mm@40407000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40407000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_mc_s2mm";
            interrupts = <0 53 4>; // PL Interrupt 85 (53 = 85 - 32)
            dma-data-mem-size = <0x1000000>; /* Big Endian */
            dma-descriptors-mem-size = <0x40000>; /* Big Endian */
    	};
        axi_dma_loop_mc_combined: axi_dma_loop_mc_combined@40408000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40408000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_mc_combined";
            interrupts = <0 54 4>; // PL Interrupt 86 (54 = 86 - 32)
            dma-data-mem-size = <0x1000000>; /* Big Endian */
            dma-descriptors-mem-size = <0x40000>; /* Big Endian */
    	};
	};
};


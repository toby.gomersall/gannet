Description
===========

The four directories in this reference implementation demonstrate a complete
framework for libgannet, and is the system that is used for the libgannet tests
(that is, to run the libgannet tests, you should have a loopback system
set up as described in this implementation.

Additional READMEs and build instructions are found in the sub-directories.

The directories are:

 * `dma_loopback_pl` with a `.tcl` scripts to build a suitable block diagram.
 * `device_tree` contains a device tree with parts specific to the loopback
 PL and necessary for the test hardware, giving a useful example of how the
 device tree should be created, as part of a specific implementation of the
 device tree for a Zedboard and a Picozed on FMC card
 * `driver` contains a linux device driver to expose the hardware to userspace
 through the UIO system. Libgannet expects this driver to be installed.
 * `udev_rules` which contains a udev rules file for setting up suitable
 symlinks for the test system (as well as providing a generally useful 
 example).


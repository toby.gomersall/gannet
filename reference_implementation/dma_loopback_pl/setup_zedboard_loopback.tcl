set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project zedboard_loopback zedboard_loopback -part xc7z020clg484-1
   set_property BOARD_PART em.avnet.com:zed:part0:1.3 [current_project]
}

source zedboard_loopback.tcl

make_wrapper -files [get_files zedboard_loopback/zedboard_loopback.srcs/sources_1/bd/loopback/loopback.bd] -top
add_files -norecurse zedboard_loopback/zedboard_loopback.srcs/sources_1/bd/loopback/hdl/loopback_wrapper.v
set_property STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE true [get_runs impl_1]

This README file contains information on the contents of libgannet.

Please see the corresponding sections below for details.

This is designed to run on an embedded Linux system. In our implementation we
used Yocto Jethro with the meta-xilinx layer and some custom add ons (which do
not affect libgannet):

    URI: git://git.yoctoproject.org/meta-xilinx
    branch: jethro


# Dependencies

This library depends on:


# Patches

Please submit any patches against libgannet to the maintainer:

Maintainer: Toby Gomersall <toby.gomersall@smartacoustics.co.uk>


# Table of Contents

1. FPGA binary
2. Examples
3. Testing
4. Building
5. Development and usage
6. Licensing

# FPGA binary

The vivado project can be recreated using the tcl script:

    libgannet/reference_implementation/dma_loopback_pl/loopback.tcl

This can then generate the FPGA binary required for the examples and tests.


# Examples

Example code can be found in the examples directory. This can be run using:

`cargo run --example main`

# Testing

To run the tests:

`cargo test`

If you want to print debug from the code during the tests run:

`cargo test -- --nocapture`

To run a specific test:

`cargo test -- <test name>`

To run the tests under valgrind after building for debug:

`valgrind ./libgannet/target/debug/gannet`

To run the tests under valgrind after building for release:

`valgrind ./libgannet/target/release/gannet`

# Building

To build for debug:

`cargo build`

To build for release:

`cargo build --release`

# Development and Usage

Please see this <a href="https://tobygomersallwordpresscom.wordpress.com/2016/02/11/xilinx-zynq-using-dma-to-transfer-data-to-the-linux-userspace/">blog post</a> for an explanation of the design.

There are examples of how to use the library in tests and libgannet/examples/main.rs. Below is a brief description of the key points.

It is necessary to know the device type before attempting any operations with the DMA device. These device types are:

- Direct (See [product guide](https://www.xilinx.com/support/documentation/ip_documentation/axi_dma/v7_1/pg021_axi_dma.pdf))
- Scatter Gather (See [product guide](https://www.xilinx.com/support/documentation/ip_documentation/axi_dma/v7_1/pg021_axi_dma.pdf))
- Multi Channel (See [product guide](https://www.xilinx.com/support/documentation/ip_documentation/axi_mcdma/v1_0/pg288-axi-mcdma.pdf))

## Direct devices

1. Create the device: `let device = direct::Device::new(<device file path>)`. This will return a Device object.
2. Use `device.get_memory()` to get a pointer to and the size of the memory to which the device has access. Your application can then write to or read from this memory.
3. To perform a transfer, use `device.do_dma(&nbytes, &offset)` where nbytes is the number of bytes to transfer and offset is the address that the device will read from (MM2S) or write to (S2MM).
4. Call `device.wait_transfer_complete(timeout)` where timeout is the maximum length of time to wait for the transfer to finish. If the transfer was successful, this will return the number of bytes transferred. If the transfer failed, this function will return an error. The application should always call `wait_transfer_complete` after calling `do_dma`.
5. If an error is ever raised, run `device.reset()` before calling any further functions on the device.

## Scatter Gather devices

1. Create the device: `let device = scatter_gather::Device::new(<device file path>)`. This will return a Device object.
2. Use `device.get_memory()` to get a pointer to and the size of the memory to which the device has access. Your application can then write to or read from this memory.
3. Create operations for the device to run by calling `let operation = new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)`. This will create a DMA operation to write `n_blocks` of `block_size` bytes into the memory at `offset` with `block_stride` bytes between the start of each block. This call will return an Operation object.
4. To perform a transfer, use `device.do_dma(&operation)`.
5. Call `device.wait_transfer_complete(timeout)` where timeout is the maximum length of time to wait for the transfer to finish. If the transfer failed, this function will return an error. The application should always call `wait_transfer_complete` after calling `do_dma`.
6. Before running any more transfers with the operation the application must call `check_and_refresh_operation(operation)`.

## Multi Channel devices

1. Create the device: `let device = multi_channel::Device::new(<device file path>)`. This will return a Device object.
2. Use `device.get_memory()` to get a pointer to and the size of the memory to which the device has access. Your application can then write to or read from this memory.
3. Create operations for the device to run by calling `let operation = new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)`. This will create a DMA operation to write `n_blocks` of `block_size` bytes into the memory at `offset` with `block_stride` bytes between the start of each block. This call will return an Operation object.
4. To perform a transfer, use `device.do_dma(&operations)` where operations is a hashmap of operations (created in step 3) with the channel number (0 - 15) as the key. Note: For S2MM transfers the transfer should be set up in advance of the data arriving, otherwise there is a risk of packets being dropped silently.
5. Call `device.wait_transfer_complete(timeout)` where timeout is the maximum length of time to wait for the transfer to finish. If the transfer failed, this function will return an error. The application should always call `wait_transfer_complete` after calling `do_dma`.
6. Before running any more transfers with the operations the application must call `check_and_refresh_operation(operation)` on every operation that was used in the DMA transfer.


# Licensing

This library is licensed under the GPL. Commercial licenses can be purchased for a one time fee of £10,000 (plus VAT). Please contact Toby Gomersall (<toby.gomersall@smartacoustics.co.uk>).
